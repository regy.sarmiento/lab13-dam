import 'package:lab13flutter/src/pages/home_temp.dart';
import 'package:lab13flutter/src/pages/alert_page.dart';
import 'package:lab13flutter/src/pages/avatar_page.dart';
import 'package:lab13flutter/src/pages/card_page.dart';
import 'package:lab13flutter/src/pages/animated_page.dart';
import 'package:lab13flutter/src/pages/input_page.dart';
import 'package:lab13flutter/src/pages/slider_page.dart';
import 'package:lab13flutter/src/pages/list_page.dart';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Material App",
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => HomePageTemp(),
        'alert': (context) => AlertPage(),
        'avatar': (context) => AvatarPage(),
        'card': (context) => CardAndTarjetPage(),
        'animatedContainer': (context) => AnimatedContainerPage(),
        'inputs': (context) => InputsPage(),
        'sliderAndChecks': (context) => SliderAndCheckPage(),
        'listAndScroll': (context) => ListAndScrollPage(),
      },
    );
  }
}
