import 'package:flutter/material.dart';

class CardAndTarjetPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards - Tarjetas'),
        backgroundColor: Colors.red,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Inicio Ejemplo Card
            Card(
              child: Column(
                mainAxisSize: MainAxisSize.min, 
                children: <Widget>[
                  const ListTile(
                    leading: Icon(Icons.album),
                    title: Text('The Enchanted Nightingale'),
                    subtitle:
                        Text('Music by Julie Gable. Lyrics by Sidney Stein.'),
                  ),
                  ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text('BUY TICKETS'),
                        onPressed: () {},
                      ),
                      FlatButton(
                        child: const Text('LISTEN'),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // Fin Ejemplo Card
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              child: Icon(Icons.arrow_back),
              backgroundColor: Colors.red,
              onPressed: () {
                Navigator.of(context).pop(true);
              })
        ],
      ),
    );
  }
}
