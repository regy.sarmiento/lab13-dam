import 'package:flutter/material.dart';

class InputsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs'),
        backgroundColor: Colors.red,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Input N°1",style: TextStyle( fontSize: 25)),
            TextField(
              decoration: InputDecoration(
                  border: InputBorder.none, hintText: 'Username'),
            ),
            Text("Input N°2", style: TextStyle( fontSize: 25)),
            TextFormField(
              onChanged: (text) {
                print("");
              },
              decoration: InputDecoration(labelText: 'Username'),
            )
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              child: Icon(Icons.arrow_back),
              backgroundColor: Colors.red,
              onPressed: () {
                Navigator.of(context).pop(true);
              })
        ],
      ),
    );
  }
}
